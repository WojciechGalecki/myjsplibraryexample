<%@ page import="pl.sda.library.Category" %>
<%@ page import="pl.sda.library.Book" %>
<%@ page import="pl.sda.library.Author" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.HashSet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Książki</title>
</head>
<body>

<c:choose>
    <c:when test="${param.action eq 'add'}">
        <% addBook(request, session); %>
    </c:when>
    <c:when test="${param.action eq 'edit'}">
        <% editBook(request, session); %>
    </c:when>
    <c:when test="${param.action eq 'remove'}">
        <% removeBook(request, session); %>
    </c:when>
</c:choose>

<c:choose>
    <c:when test="${param.view eq 'list'}">
        <jsp:forward page="book/book_list.jsp"></jsp:forward>
    </c:when>
    <c:when test="${param.view eq 'add_form'}">
        <jsp:forward page="book/book_add_form.jsp"></jsp:forward>
    </c:when>
    <c:when test="${param.view eq 'edit_form'}">
        <jsp:forward page="book/book_edit_form.jsp"></jsp:forward>
    </c:when>
    <c:otherwise>
        <jsp:forward page="book/book_list.jsp"></jsp:forward>
    </c:otherwise>
</c:choose>

</body>
</html>
<%!
    private void addBook(HttpServletRequest request, HttpSession session) {
        String title = request.getParameter("title");
        // pobranie kategorii przez request????
        Category category = (Category)request.getAttribute("categories");
        int year = Integer.valueOf(request.getParameter("year"));
        Set<Author> authors = new HashSet<>();
        // jak pobrac autorów z checkboxa????
        Book book = new Book(title,category,authors,year);

        Map<Integer, Book> books = (Map<Integer, Book>) session.getAttribute("books");
        if (books == null) {
            books = new HashMap<>();
        }
        books.put(book.getId(), book);
        session.setAttribute("books", books);

    }

    private void editBook(HttpServletRequest request, HttpSession session) {

    }

    private void removeBook(HttpServletRequest request, HttpSession session) {

    }
%>