<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Książki</title>
</head>
<body>

<c:choose>
    <c:when test="${param.action eq 'add'}">
        <% addBook(request, session); %>
    </c:when>
    <c:when test="${param.action eq 'edit'}">
        <% editBook(request, session); %>
    </c:when>
    <c:when test="${param.action eq 'remove'}">
        <% removeBook(request, session); %>
    </c:when>
</c:choose>

<c:choose>
    <c:when test="${param.view eq 'list'}">
        <jsp:forward page="book/book_list.jsp"></jsp:forward>
    </c:when>
    <c:when test="${param.view eq 'add_form'}">
        <jsp:forward page="book/book_add_form.jsp"></jsp:forward>
    </c:when>
    <c:when test="${param.view eq 'edit_form'}">
        <jsp:forward page="book/book_edit_form.jsp"></jsp:forward>
    </c:when>
    <c:otherwise>
        <jsp:forward page="book/book_list.jsp"></jsp:forward>
    </c:otherwise>
</c:choose>

</body>
</html>
<%!
    private void addBook(HttpServletRequest request, HttpSession session) {

    }

    private void editBook(HttpServletRequest request, HttpSession session) {

    }

    private void removeBook(HttpServletRequest request, HttpSession session) {

    }
%>