<%@ page import="java.time.LocalDate" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Dodawanie książki</title>
</head>
<body>
<%@include file="../common/menu.jspf" %>
<fieldset>
    <legend>Dodawanie książki</legend>

    <form action="<c:url value="../book.jsp"/>" method="post">
        <input type="hidden" name="action" value="add"/>
        <table border="0">
            <tr>
                <td>Tytuł:</td>
                <input type="text" name="title" value="${book.title}"/><br/>
            </tr>
            <tr>
                <td>Kategoria:</td>
                <td><select name="category">
                    <c:forEach items="${categories}" var="category">
                        <c:set var="selected" value=""/>
                        <c:if test="${category.value.id eq book.category.id}">
                            <c:set var="selected" value="selected='selected'"/>
                        </c:if>
                        <option value="${category.value.id}" ${selected}>${category.value.name}</option>
                    </c:forEach>
                </select>
                </td>
            </tr>
            <tr>
                <td>Autorzy:</td>
                <td>
                    <c:forEach items="${authors}" var="author">
                        <c:set var="checked" value=""/>
                        <c:forEach items="${book.authors}" var="bookAuthors">
                            <c:if test="${author.value.id eq bookAuthors.id}">
                                <c:set var="checked" value="checked='checked'"/>
                            </c:if>
                        </c:forEach>
                        <input type="checkbox" name="authors"
                               value="${author.value.id}" ${checked}> ${author.value.name} ${author.value.surname} <br/>
                    </c:forEach>
                </td>
            </tr>
            <tr>
                <td>Rok wydania:</td>
                <input type="number" name="year" min="1" max="<%LocalDate.now().getYear();%>" value="${book.year}"/>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="submit" value="zapisz"/></td>
            </tr>
        </table>
    </form>
</fieldset>
</body>
</html>
