<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Lista autorów</title>
</head>
<body>
<%@include file="../common/menu.jspf"%>
<fieldset>
    <legend>Lista autorów</legend>
    <c:choose>
        <c:when test="${empty authors}">
            <b style="color: red"><c:out value="Brak autorów" /></b>
        </c:when>
        <c:otherwise>
            <table border="0">
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>surname</th>
                    <th>akcja</th>
                </tr>
                <c:forEach var="authors" items="${authors}">
                    <tr> <!-- Map<Integer, Author> -->
                        <td><c:out value="${authors.value.id}"></c:out></td>
                        <td><c:out value="${authors.value.name}"></c:out></td>
                        <td><c:out value="${authors.value.surname}"></c:out></td>
                        <td>
                            <a href="<c:url value="../author.jsp"><c:param name="id" value="${authors.value.id}" /><c:param name="view" value="edit_form"/></c:url>">edytuj</a>
                            <a href="<c:url value="../author.jsp"><c:param name="id" value="${authors.value.id}" /><c:param name="action" value="remove"/></c:url>">usuń</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:otherwise>
    </c:choose>
    <p>
        <a href="<c:url value="../author.jsp"><c:param name="view" value="add_form"/></c:url>">Dodaj autora</a>
    </p>
</fieldset>
</body>
</html>
